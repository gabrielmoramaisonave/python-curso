#funcion que regresa suma, division, etc. de valores

def suma(a, b, c):
    return a + b + c

def division (a,b):
    return a/b

def multiplicacion (a,b=10): #si y solo si no esta el segundo argumento, toma b como 10
    return a*b

def multiples_valores(): #es para cuando tengo que regresar más de un valor, solo los separo por ,
    return "string", 1, True, 25.6 #me retorna una tupla con los valores


resultado = multiples_valores() #si quiero puedo colocar primero el valor dos y despues el uno, para eso
#tengo que poner (b=10, a=100)
string, entero, bol, floa = multiples_valores() #al ser una tupla de esta forma los acomodo y puedo imprimir despues
#maximizo de esa forma, al no tener que definir uno por uno posicion como si fuera arrays, lo acomoda solo así"

mi_variable = multiplicacion #asigno la funcion a una variable para que la ejecute cuando quiera
resultado = mi_variable(6,8) #ejecuto la funcion mediante la variable
#print (resultado)

'''
print(string)
print(entero)
print(bol)
print(floa)
'''

#podemos usar funciones dentro de otras funciones

def mostrar_resultado (funcion): #recibe una funcion que se la asigne abajo
    print(funcion(6,8)) #la ejecuto con los parametros que quiera

mi_variable =multiplicacion
mostrar_resultado(mi_variable) #asigne la funcion mi variable (que es multiplicar)

