#Un decorador nos deja agregar mayor funcionalidad a una funcion en concreto
# un decorador es una funcion que recibe como parametro otra funcion para dar como salida una nueva funcion
#A, B, C son funciones
#A recibe como parametro B para poder crear C
#debo decir que mi funcion trabaja con n cantidad de argumentos

''' DE ESTA FORMA QUEDA FLEXIBLE PARA RECIBIR O NO ARGUMENTOS '''

def decorador(func): #A, B
    def nueva_funcion(*args,**kwargs):#aca usamos *args y **kwargs para decir q trabaja con n cantidad de argumentos
        #Agregue Codigo Ej: Abrir base de dato
        print("vamos a ejecutar la funcion")
        func(*args,**kwargs) #aca tambien van los *args y **kwargs
        #Agregue codigo Ej: Cerra base de datos luego de la peticion
        print("Se ejecuto la funcion")
    return nueva_funcion #C

@decorador #con el @ decoro lo que esta abajo del decorador es lo que entra decorado
def saluda():
    print("Hola Mundo")
saluda ()

@decorador
def suma(num_uno, num_dos):
    print(num_uno + num_dos)
suma(80,17)