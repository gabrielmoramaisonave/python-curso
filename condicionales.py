fruta = "manzana"

if fruta == "kiwis" :
    print("El valor es kiwi")

elif fruta == "manzana":     #Para condicionar fruta como kiwi o manzana
    print("Es una manzana")
#puedo tener los elif que quiera

# con pass, me permite colocar la condicion pero no la accion, se da en los casos que no se que accion va a tener al darse la condicion
elif fruta == "manzana":    
    pass
else:
    print("No son iguales")

# mensaje = "El valor es kiwi" if fruta == "kiwi" else "No son" #Forma en una linea para el if

#Variables Booleanas IF
#Las variables vacias o nulas van a ser False, ejemplo una lista vacia [].

#if [], (), {}, 0, "" todos estas variables van a ser falsas, es decir False 

#Las variables que tengan valor seran True

#True = 1
#False = 0

if 0: #si if=1 me imprime el if, si if=0 me imprime el else
    print("Es verdadero")
else:
    print("Es Falso")
    
#esto nos sirve mucho para saber si una variable tiene valor o esta vacia. Lo comprobamos con if, si tiene valor sale por if, sino por else.

#la palabra None, sirve para decir que una variable no tiene valor.
