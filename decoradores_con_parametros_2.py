''' DE ESTA FORMA QUEDA FLEXIBLE PARA RECIBIR O NO ARGUMENTOS '''

def decorador(func): #A, B
    def nueva_funcion(*args,**kwargs):#aca usamos *args y **kwargs para decir q trabaja con n cantidad de argumentos
        #Agregue Codigo Ej: Abrir base de dato
        print("vamos a ejecutar la funcion")
        resultado = func(*args,**kwargs) #aca tambien van los *args y **kwargs - Guardo en una variable para que me imprima abajo
        #Agregue codigo Ej: Cerra base de datos luego de la peticion
        print("Se ejecuto la funcion")
        return resultado
    return nueva_funcion #C

@decorador #con el @ decoro lo que esta abajo del decorador es lo que entra decorado
def saluda():
    print("Hola Mundo")
saluda ()

@decorador
def suma(num_uno, num_dos):
    return(num_uno + num_dos) #aca me retorna pero no me imprime
resultado = suma(80,17) #entonces guardo el resultado de la suma en una variable, en este caso resultado
print (resultado) #imprimo la variable
