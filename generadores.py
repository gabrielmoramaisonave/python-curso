#Los generadores nos sirven para crear objetos sin necesidad de almacenarlo en la memoria ram

''' IMPORTANTE NO USAR RETURN, USAR YIELD '''

# yield puede regresar varios valores, ej: yield valor *10, True
def generador(*args):
    for valor in args:
        yield valor * 10 #un ejemplo

''' SI RECIBE DOS VALORES, EL FOR TIENE QUE ESTAR PARA DOS. 

Ej: for valor_uno, valor_dos in generador(1,2,3,4,5,6,7,8,9)
        print(valor_uno, valor_dos)''' 

for valor in generador(1,2,3,4,5,6,7,8,9):
    print(valor)