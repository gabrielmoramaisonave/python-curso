#closure es una funcion que crea otra funcion

def crear_funcion(num_uno, num_dos): #esta funcion ejecuta la primera

    #al ser anidada la funcion, validacion toma los argumentos de division.
    def validacion(): #esta validad por verdadero o falso
        print("Se ejecuta validacion")
        return num_uno > 0 and num_dos > 0
    return validacion

def aplicar_funcion(func): #clouser con funcion que recibe como argumento otra funcion
    func()


nueva_funcion = crear_funcion(10, -5) #crear funcion me crea una nueva funcion y la guarda en nueva_funcion
#print(nueva_funcion())#aca no coloco nuevos argumentos, porque ya lo hice crear funcion - forma simple de ejecutar
aplicar_funcion(nueva_funcion) #aca la ejecuto.