#Un decorador nos deja agregar mayor funcionalidad a una funcion en concreto
# un decorador es una funcion que recibe como parametro otra funcion para dar como salida una nueva funcion
#A, B, C son funciones
#A recibe como parametro B para poder crear C

def decorador(func): #A, B
    def nueva_funcion():
        #Agregue Codigo Ej: Abrir base de dato
        print("vamos a ejecutar la funcion")
        func()
        #Agregue codigo Ej: Cerra base de datos luego de la peticion
        print("Se ejecuto la funcion")
    return nueva_funcion #C

@decorador #con el @ decoro lo que esta abajo del decorador es lo que entra decorado
def saluda():
    print("Hola Mundo")
saluda ()

@decorador
def suma():
    print(10+20)
suma()