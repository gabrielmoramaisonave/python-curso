#Las comprehension son features, nos permiten crear listas, diccionario y tuplas de forma sencilla
lista = [ valor for valor in range(0,101) if valor % 2 ==0 ] #le agregue el condicional para q imprima los pares
# lista = [ valor(esto es para q lo guarde, no hace falta q sea lo mismo q corra en el ciclo) for valor in rango (0,101) (este es el ciclo) ]
#puedo usar condicionales adentro.

tupla = tuple (( valor for valor in range(0,101) if valor % 2 !=0 )) #hay que agregar la funcion tuple a todo lo anterior
diccionario = { indice: valor for indice, valor in enumerate (lista) } #lo que hice fue que pedi que se cree un diccionario donde el indice se llame indice, y el valor valor.

print(tupla)
print (diccionario)
