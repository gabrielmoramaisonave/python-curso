#las variables dentro de una funcion son locales, solo existen dentro de la funcion, no modifican las que estan afuera
#Las que estan afuera de la funcion son variables globales

def palindromo(frase):
    frase = frase.replace(' ', '') #me reemplaza los espacios por aire
    return frase == frase[::-1]#me escribe al revez la frase

frase = 'anita lava la tina'
resultado = palindromo(frase)
print(resultado)

