#para modificar una variable global dentro de una funcion tengo que usar la palabra reservada global + nombre de variable

def valor_global():
    global variable_gloabal
    variable_gloabal = 'Cambiar valor'

variable_gloabal = 'Esto es una variable global'
print(variable_gloabal)
valor_global()#llamo funcion así que me tiene que cambiar el string
print(variable_gloabal)

#una funcion puede crear una variable global

def crear_global():
    global nueva_variable
    nueva_variable = 'Esto es una variable global'

crear_global()
print(nueva_variable)