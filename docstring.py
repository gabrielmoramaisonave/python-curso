#Hay que documentar las funciones, en la primera linea de la funcion
#en la primera linea de la funcion el interprete lo toma como la documentacion
#las funciones son clases, entonces doc es un atributo

def generador(*args):
    ''' Recibe n cantidad de números y regresa el numero ademas de regresar
    True o False si el numero es mayor a 5
     '''
    for valor in args:
        yield valor, True if valor > 5 else False

nombre = generador.__name__ #name es el atributo de nombre
documentacion = generador.__doc__ #doc va a tener el valor de lo que esta en al triple comillas
print(nombre, " : ")
print(documentacion)
