''' Cuando abstraemos el objeto creamos la clase que nos permite crear n cantidad de objetos'''

'''Plantilla de clase'''

class Lapiz: #comienza con mayuscula y no separamos con guion bajo, van juntas
    color = "Amarillo" #aca van todos los atributos
    contiene_borrador = False
    usa_grafito = True

'''Crear Objeto'''

lapiz_generico = Lapiz() #nombre = nombre de clase + ()
print(lapiz_generico.color) #llamo el atributo que quiero imprimir 
