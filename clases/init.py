#El metodo Init sirve para que los objetos arranquen con atributos preestablecidos

class Lapiz: 

    def __init__(self): #con este metodo pongo atributos por default
        self.color = "Amarillo"  #aca los inicializo
        self.contiene_borrador = False #van con self. primero porque asi los llamo  
        self.usa_grafito = True

    def dibujar(self): 
        print("El lapiz esta dibujando.")
    
    def borrar(self):
        if self.es_valido_para_borrar():
            print("El lapiz esta borrando.")
        else:
            print("No es posible borrar")

    def es_valido_para_borrar(self):
        return self.contiene_borrador 


lapiz_generico = Lapiz() 
lapiz_generico.contiene_borrador = True
lapiz_generico.dibujar() 
lapiz_generico.borrar()
