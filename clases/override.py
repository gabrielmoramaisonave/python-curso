#Sobreescritura de metodos, es cuando dentro de una clase creamos un metodo con el mismo nombre de la clase padre
#Primero busca dentro de la clase el metodo, sino lo encuentra busca en las heredadas.
#Se complementan o lo sustituye dependiendo el caso

class Mascota:
    nombre = ""#Todas las mascotas necesitan un nombre
    def mostrar_nombre(self):
        print(self.nombre)


class Animal:
    @property
    def terrestre(self):
        return True

class Felino(Animal): #Clase padre

    @property
    def garras_retractiles(self):
        return True

    def cazar(self):
        print("El felino esta cazando")

class Gato(Felino, Mascota):#separo las clases padres con coma

    def mostrar_nombre(self): #sobreescribir metodo de clase mascota
        Mascota.mostrar_nombre(self)#si quiero tambien que se ejecute el metodo de la clase padre
    #lo llamo con el nombre de la clase.nombre del metodo (self)
        print("El nombre del gato es : {}".format(self.nombre)) #ejecuta el metodo dentro de la clase en lugar de la instancia


gato = Gato()
gato.nombre = "Jorge Luis"
gato.mostrar_nombre()

