#sirve para que los atributos no se modifiquen en la instancia, solo en la clase

class Usuario:
    def __init__(self, username, password, email):
        self.username = username
        self.__password = self.__generar_password(password) #con doble __ ya queda como privado, por lo tanto las instancias no lo pueden modificar
        self.email = email
    
    def __generar_password(self, password): #tambien puedo generar metodos privados
        return password.upper() #simulamos encriptacion pasando a mayuscula

    def get_password(self): #como es atributo privado la password, necesito un metodo que me la retorne para poder imprimirla despues
        return self.__password

eduardo = Usuario("Eduardo", "eduardo123", "eduardo@ubuntu.com")
print(eduardo.username)
print(eduardo.get_password())
print(eduardo.email)
 