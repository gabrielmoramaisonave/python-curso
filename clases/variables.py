#las variables de clase, les pertenencen a la clase y no a los objetos
#Puedo usar las variables de clase en metodos. Lo hacemos con self.

class Circulo:

    _pi = 3.1416 #va antes que los metodos porque es variable de clase
    #le ponemos un _ antes del nombre para que no la modifiquen. (convencion de developer)

    def __init__(self, radio):
        self.radio = radio
        

circulo_uno = Circulo(4)
circulo_dos = Circulo(3)

print(Circulo.pi) #No necesito crear un objeto para usar PI, porque es de class

print(circulo_uno.pi) #lo puedo usar en los objetos porque es de class
print(circulo_dos.pi)

print(circulo_uno.__dict__)#me imprime un diccionario con todos los atributos del objeto. Si no aparece aca
#es porque es variable de clase.