#Los properties son para visualizar y modificar los atributos privados (retornar)
#property = visualizar
# setter = modificar

class Usuario:
    def __init__(self, username, password, email):
        self.username = username
        self.__password = self.__generar_password(password)
        self.email = email
    
    def __generar_password(self, password): 
        return password.upper() 
    
    @property
    def password(self):
        return self.__password

    @password.setter #sirve para modificar atributo privado
    def password(self, valor):
        self.__password = self.__generar_password(valor)

eduardo = Usuario("Eduardo", "eduardo123", "eduardo@ubuntu.com")
print(eduardo.password)
eduardo.password = "Nuevo Password"
print(eduardo.password)