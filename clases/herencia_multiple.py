# pueden heredar de distintos padres

class Mascota:
    nombre = ""#TOdas las mascotas necesitan un nombre
    def mostrar_nombre(self):
        print(self.nombre)


class Animal:
    @property
    def terrestre(self):
        return True

class Felino(Animal): #Clase padre

    @property
    def garras_retractiles(self):
        return True

    def cazar(self):
        print("El felino esta cazando")

class Gato(Felino, Mascota):#separo las clases padres con coma
    pass 
class Jaguar(Felino):
    pass
    

gato = Gato()
gato.nombre = "Jorge Luis"
gato.mostrar_nombre()
print(gato.terrestre)
