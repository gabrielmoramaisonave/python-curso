

class Lapiz: 
    color = "Amarillo" #Atributos
    contiene_borrador = False
    usa_grafito = True

#los metodos son funciones dentro de las clases, es decir las acciones que va a ejecutar el objeto
#Metodo 
    def dibujar(self): #dentro de atributos va la palabra reservada self
        print("El lapiz esta dibujando.")
    
    def borrar(self):
        if self.es_valido_para_borrar(): #para llamar a un metodo va self.nombre met()
            print("El lapiz esta borrando.")
        else:
            print("No es posible borrar")

    def es_valido_para_borrar(self):
        return self.contiene_borrador #para llamar un atributo dentro del metodo pongo self.nombre del atributo


lapiz_generico = Lapiz() #nombre = nombre de clase + ()
lapiz_generico.contiene_borrador = True
lapiz_generico.dibujar() #para llamar el metodo va con el nombre y ()
lapiz_generico.borrar()
 #al ser atributo puedo cambiarle el valor

#dentro de un metodo puedo mandar a ejecutar otro metodo