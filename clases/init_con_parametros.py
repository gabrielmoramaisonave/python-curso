#El metodo Init sirve para que los objetos arranquen con atributos preestablecidos

class Lapiz: 

    def __init__(self, color, contiene_borrador, usa_grafito): #de esta forma los dejo preestablecido para cargarlos cuando instancie el objeto
        self.color = color
        self.contiene_borrador = contiene_borrador
        self.usa_grafito = usa_grafito

    def dibujar(self): 
        print("El lapiz esta dibujando.")
    
    def borrar(self):
        if self.es_valido_para_borrar():
            print("El lapiz esta borrando.")
        else:
            print("No es posible borrar")

    def es_valido_para_borrar(self):
        return self.contiene_borrador 


lapiz_generico = Lapiz("Verde", False, True) #aca tengo que instanciar los atributos
lapiz_generico.dibujar() 
lapiz_generico.borrar()