#Los creamos cuando algo muy puntual pertenece a toda una clase y no solo a un objeto, en este caso PI

class Circulo:

    @staticmethod
    def pi(): #metodo estatico (no tiene self, esta dentro de la clase y decorado)
        return 3.1416 #los metodos estaticos le pertenecen a la clase, ergo puedo llamarlo sin objeto

    def __init__(self, radio):
        self.radio = radio
    
    def area(self): #metodos de instancia (siempre que diga self y este dentro de la clase)
        return self.radio * self.radio * Circulo.pi()
print(Circulo.pi())
circulo_uno = Circulo(7)
print(circulo_uno.area())