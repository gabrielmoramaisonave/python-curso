#un mecanismo que nos deja crear una clase a partir de una clase existente
#Puedo usar los metodos publicos en las clases o instancias
#Si es un metodo privado no se puede heredar
#Las clases padres pueden heredar de otras clases. En este caso por ejemplo que felino herede de animal
class Felino: #Clase padre

    @property
    def garras_retractiles(self):
        return True

    def cazar(self):
        print("El felino esta cazando")

class Gato(Felino):#coloco entre parentesis que Clase quiero heredar
    pass 
class Jaguar(Felino):
    pass
    

gato = Gato()
jaguar = Jaguar()

print(gato.garras_retractiles)
print(jaguar.garras_retractiles)