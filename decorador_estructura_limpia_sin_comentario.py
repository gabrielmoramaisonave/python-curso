def decorador(func):
    def before_action():
        print("Vamos a ejecutar la función")
    
    def after_action():
        print("Se ejecuto la función")
    
    def nueva_funcion(*args, **kwargs):
        before_action()
        resultado = func(*args, **kwargs) #la ejecuto
        after_action()
        return resultado
    return nueva_funcion

@decorador #con el @ decoro lo que esta abajo del decorador es lo que entra decorado
def saluda():
    print("Hola Mundo")
saluda ()

@decorador
def suma(num_uno, num_dos):
    return(num_uno + num_dos) #aca me retorna pero no me imprime
resultado = suma(80,17) #entonces guardo el resultado de la suma en una variable, en este caso resultado
print (resultado) #imprimo la variable
