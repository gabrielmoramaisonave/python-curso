def suma (valor_uno, valor_dos):
    return valor_uno + valor_dos

resultado = suma(10, 50)
print(resultado)

def suma2 (*argumento): #al poner * le digo al codigo que puede recibir n cantidad de argumentos
    print(argumento)
    print(type(argumento)) # lo va a trabajar como tupla por defecto

resultado = suma2(10, 50, 60,5,6,7,9)
print(resultado)

# a los argumentos se los nombra como args 
#con esta funcion hago la suma de todos los argumentos, lo hago con un for
def suma3 (*argumento): #al poner * le digo al codigo que puede recibir n cantidad de argumentos
    resultado = 0
    for valor in argumento:
        resultado = resultado + valor
    return resultado

resultado = suma3(1,2,3,4,5,6,7,8,9)
print(resultado)


#con esta funcion es para poner n argumentos pero con variables definidas
def suma4(**kwargs):
    valor= kwargs.get("valor", "No contiene el valor")
    print(valor)

resultado = suma4(valor = "eduardo", x = 2, y = 9, z = True ) #de esta forma lo devuelve como un diccionario
print(resultado)

# * -> n valores -> tuplas
# ** -> n valores -> diccionarios