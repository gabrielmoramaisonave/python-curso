#while

#while condicion:
#tambien puedo ponerle else abajo para que me diga que ejecutar si no se cumple la condicion

contador = 0
while contador < 10:
    print (contador)
    contador = contador + 1 #Aumenta para que no sea bucle infinito 
    if contador == 5:
        continue 
    if contador == 6:
        break #Lo corta cuando sea 6
else: 
    print("El while a terminado") 

#podemos poner condicionales, tuplas, listas, etc dentro del while

#el continue nos sirve para que el ciclo se siga ejecutando y el break lo termina
#Puedo usar flags tambien:
# flag= true
# while flag
#y cuando quiero cortar flag pasa a ser false