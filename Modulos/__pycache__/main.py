#Tienen que estar al mismo nivel (en las carpetas). Importo el archivo sin la extension
#la metadata name siempre va a ser el del scrip principal


import calculadora

resultado = calculadora.suma(30, 45) #uso con el nombre del archivo. funcion
print(resultado)

''' OTRA FORMA DE USAR FUNCIONALIDADES

from calculadora import suma, resta, multiplicacion, division (importe todas las funciones, pero
puedo importar las que quiero)

resultado = suma (30, 45)
print(resultado)

'''

''' CON LA PALABRA RESERVADA as PUEDO CAMBIAR EL NOMBRE A LA FUNCIONALIDAD

from calculadora import resta as R1

resultado = R1 (30, 45)
print(resultado) '''


''' PARA IMPORTAR TODAS LAS FUNCIONES USO *

from calculador import *

'''

''' SI QUEREMOS QUE UN SCRIP EJECUTE ALGO CUANDO ES PRINCIPAL PERO NO CUANDO SE IMPORTA
HACEMOS ESTO:

ej: def saluda():
        print("Saluda")
    
    if __name__ == '__main__'
        saluda()

BASICAMENTE VALIDAMOS SI ES LA FUNCION PRINCIPAL