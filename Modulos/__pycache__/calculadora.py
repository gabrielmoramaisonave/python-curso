#Los modulos son archivos con extensiones .py los cuales pueden ser utilizados dentro de otro archivo
#la metadata name siempre va a ser el del scrip principal

#!/usr/bin/python3   (esto es para saber que interprete utiliza)

'''
Aqui colocamos todo lo que hace el modulo a que contexto le corresponde
'''
#despues van estas metadatas para documentar bien nuestro modulo
__autor__ = "Gabriel Santiago Mora Maisonave"
__copyright__= "Copyright 2020, TUP"
__credits__ = "TUP 2020"
__license__ = "GPL"
__version__ = "1.0.1"
__maintainer__ = "Gabriel Santiago"
__email__ = "gabrielmoramaisonave@gmail.com"
__status__= "Developer"

def suma(num_uno, num_dos):
    '''Regresa un numero entero el cual es el resultado de una suma'''
    return num_uno + num_dos

def resta(num_uno, num_dos):
    '''Regresa un numero entero el cual es el resultado de una resta'''
    return num_uno - num_dos

def multiplicacion(num_uno, num_dos):
    '''Regresa un numero entero el cual es el resultado de una multiplicacion'''
    return num_uno * num_dos

def divison(num_uno, num_dos):
    '''Regresa un numero real el cual es el resultado de una divison'''
    return num_uno / num_dos