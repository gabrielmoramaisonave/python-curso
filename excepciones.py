 #Las excepciones son errores que van a tener nuestro script al ejecutarse, puede hacer que
 #termine de forma abrupta sino las manejamos de forma correcta

''' Usamos try y except para manejar las excepciones'''

try:
    print(2/0) #lo que se va a intentar ejecutar
except  ZeroDivisionError as ex: #aca se pone que tipo de error tenemos
    print(ex)
    print("No se puede dividir por 0") #lo que se ejecuta si falla lo del try
finally: #este bloque se ejecuta siempre
    print("Se termino el script")

''' cuando no se que tipo de excepcion es colo Exception, pero si se puntualmente cual es la coloco'''