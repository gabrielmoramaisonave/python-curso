def division(num_uno, num_dos): #esta funcion ejecuta la primera

    #al ser anidada la funcion, validacion toma los argumentos de division.

    def validacion(): #esta validad por verdadero o falso
        return num_uno > 0 and num_dos > 0

    if validacion(): #llamado de funcion
        return num_uno / num_dos

resultado = division(10, 0)
print(resultado)
