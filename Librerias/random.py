import random #trabajar con aleatorios

''' PARA NUMERO ALEATORIO'''

valor = random.randint(0,10) #le pongo de donde a donde. Toma tanto el 0 como al 10.
print(valor)

''' PARA ELEGIR ALEATORIAMENTE UN ELEMENTO DE LISTA'''

lista = [True, "String", 23, False]
valor = random.choice(lista) #para elegir un aleatorio de la lista
print(valor)

''' PARA DESORDENAR LISTA '''

lista = [True, "String", 23, False]
print(lista)
random.shuffle(lista)
print(lista)

