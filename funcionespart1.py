#funcion factorial
''' estructura de una funcion:

def + nombre de la funcion + (acá colocamos el nombre de los argumentos que queremos recibir) + :
    la funcionalidad '''

def factorial_numero(numero):
    factorial = 1
    while numero > 0:
        factorial = factorial * numero
        numero -=1
    return factorial #esto es para que me retorne un resultado
    #print (factorial) #cuando no retorna nada, con el print podemos ver el resultado en la terminal, pero no nos retorna nada

'''
como llamarla: nombre de funcion + (argumento)
'''
resultado= factorial_numero(4)
print(resultado)