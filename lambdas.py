#Sirve para crear pequeñas funciones anonimas.
#son funciones on demand
#aca hago una lambda para sumar

mi_funcion = lambda valor_uno, valor_dos: valor_uno + valor_dos
formato = lambda sentencia : '¿{}?'.format(sentencia) #hice una lambdas para darle formato de pregunta a una oracion
#despues de lambda va el numero de argumentos
#despues de los : va lo que queremos ejecutar, acciones concretas.
#en lambda no uso return, porque siempre retorna algo.
resultado = formato('Puedes hacer esto una pregunta')
print(resultado)
# si la lambda no recibe valor no pasa nada. Va el nombre = lambda : resultado
#ahora si o si tiene que regresar valor o ejecutar accion, sino tira error.
#es decir que siempre tiene que tener algo despues de los :
# aunque no tenga argumentos.